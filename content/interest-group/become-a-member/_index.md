---
title: "Become a Member"
date: 2019-04-16T16:09:45+02:00
layout: "single"
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
#header_wrapper_class: ""
#seo_title: ""
#headline: ""
#subtitle: ""
#tagline: ""
#links: []
---

# **Join Us!**

The pace of innovation in the field of mobility is faster than ever. Open source has won because no single company can compete with the scale and speed of disruptive innovation delivered by collaborative ecosystems. Join global industry leaders collaborating on a common traffic simulation and mobility modeling platform!  


### Benefits
Members of the Eclipse openMobility Interest Group take advantage of the following opportunities:

- Participate in industry collaborations to develop a common open platforms for traffic simulation and mobility modeling.
- Play a role in defining Eclipse openMobility strategic priorities
- Gain insights into the Eclipse openMobility technology roadmap and direction of associated Eclipse Projects, such as Eclipse SUMO
- Benchmark and learn best practices from peers for leveraging open simulation and modeling technologies to accelerate product development and improve time-to-revenue

### How to become a member

openMobility is open at any time to all new members. The process to participate in the openMobility interest group requires the following steps:

1. Become an Eclipse Foundation Member. The process for membership application is outlined [here](https://www.eclipse.org/membership/become_a_member/).
2. [Contact us](https://eclipse.org/collaborations/interest-groups/contact/) about joining an interest group.
