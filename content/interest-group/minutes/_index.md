---
title: "Minutes"
date: 2022-09-06T16:09:45+02:00
layout: "single"
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
#header_wrapper_class: ""
#seo_title: ""
#headline: ""
#subtitle: ""
#tagline: ""
#links: []
---

Meeting minutes:

- [Telco Nov 15, 2022](/interest-group/minutes/20221115)
- [Telco Sep 06, 2022](/interest-group/minutes/20220906)
- [Telco Jun 14, 2022](/interest-group/minutes/20220614)
- [Telco Oct 19, 2021](/interest-group/minutes/20211019)
- [Telco Oct 5, 2021](/interest-group/minutes/20211005)
- [Telco Aug 10, 2021](/interest-group/minutes/20210810)

Older minutes:

- [Archive at Eclipse Wiki](https://wiki.eclipse.org/OpenMobilityMeetingMinutes)
