---
title: "openMobility Telco Sep 6th, 2022"
date: 2022-09-06T17:09:45+02:00
layout: "single"
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
draft: false
---

## Attendees

- Robert Hilbrich 
- Michael Behrisch
- Michele Segata
- Tobias Lukowitz
- Bhanu Kala
- Sid Askary
- Philipp Heisig
- Jakob Kaths
- Wolf Kunert

## Introduction of Guests

### Seven Principles
- Tobias Lukowitz introduces 7P
- Development of products: 
   - traffic modeling based on open data and open source
   - AI based object recognition and tracking of participants to analyze the behavior of people in the mobility space
- Plan to participate in the development of TAPAS-based solutions and other parts of the product
- See more at (strazoon.com)

### Goleyo
- Bhanu Kala introduces Goleyo - a company focused on traffic modeling and simulation
- Goal: provide an integrated ecosystem of tools and models to achieve "living models" hosted on cloud services
- See more at (goleyo.com)

### Adesso
- Adesso is an IT consultancy company from Germany 
- SUMO is used in projects, e.g., AI + blender to simulate parking conditions
- Student projects: to retrieve and process data to be used in virtual scenarios
- Philipp Heisig introduces the SyntheSis project
- Remark from FutureWei: see [DORA](https://github.com/dora-rs) and [DORA-Drives](https://github.com/dora-rs/dora-drives)
- Remark from Vector: see [Dyna4](https://www.vector.com/int/en/products/products-a-z/software/dyna4/adas-testing-with-virtual-test-drives/)

## News from the Projects 

### Eclipse MOSAIC
- Berlin scenario was publicly released; see [Berlin Sumo Traffic Scenario](https://github.com/mosaic-addons/best-scenario)
- SUMO scenario with 24h demand based on MATsim data
- This simulation scenario provides motorized private transport traffic for over 24 hours for the whole city of Berlin
- With about 2,25 million trips within an area of 800 km², this is the largest microscopic traffic simulation scenario we are currently aware of

### Eclipse SUMO
- Save the date: next SUMO User Conference May 8-10, 2023 - planned to be conducted with physical presence in Berlin
- Eclipse SUMO release 1.14.1
  - HBEFA 4.2 and PHemLight 4.5 emission models integrated 
  - improved e-vehicles models
  - lots of bugfixes
- Next release is planned for early November 2022
  - new feature: integration of pedestrian simulator JuPedSim in SUMO simulations, see [talk](https://www.eclipsecon.org/2022/sessions/youll-never-walk-alone-simulating-pedestrians-world-cars)
- We would like to encourage our SUMO users to setup GitHub Actions to automatically test their scenarios with our SUMO nightly builds

## Other News
- Contribution day of software defined vehicle (22.09.22) in Bonn - see [link](https://sdv.eclipse.org)

## EclipseCon 2022

- EclipseCon 2022 will be happening as an event with physical presence see [EclipseCon website](https://www.eclipsecon.org/2022)
- Oct. 24-27 in Ludwigsburg, Germany
- Community day on the first day of the EclipseCon
- Michael Behrisch will be present at the conference to give a talk about pedestrian simulation in a world of cars, see [JuPedSim/SUMO talk](https://www.eclipsecon.org/2022/sessions/youll-never-walk-alone-simulating-pedestrians-world-cars)

## Whitepaper: Survey or Landscape of Demand Generators for SUMO

- Current status and progress
- Michael Behrisch will forward the invitations for the next meetings to SevenPrinciples and Adesso

## Future Ideas for openMobility?

- Scenario generation or standardized interfaces (e.g. openSCENARIO)
- Continue to work on Github issues [User Stories](https://github.com/openmobility-wg/userstories)
- Discussion about standardizing simulation processes and toolchains; focusing on stable APIs between tasks in the process

## Any other Business
- None
