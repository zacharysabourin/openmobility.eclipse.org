[![GitHub license](https://img.shields.io/github/license/EclipseFdn/openmobility.eclipse.org.svg)](https://github.com/EclipseFdn/openmobility.eclipse.org/blob/main/LICENSE)

# openmobility.eclipse.org

The [openmobility.eclipse.org](https://openmobility.eclipse.org) website is generated with [Hugo](https://gohugo.io/documentation/).

<img src="https://i.imgur.com/ECvSBcZ.jpg" alt="openMobility logo" width="200">

The OpenMobility interest group will drive the evolution and broad adoption of mobility modelling and simulation technologies. It accomplishes its goal by fostering and leveraging collaborations among members and by ensuring the development and availability of the “OpenMobility framework”. This framework will provide the tools for a detailed simulation of the movement of vehicles and people as well as their communication patterns. It will be valuable for testing driver assistance systems, predicting and optimizing traffic as well as evaluating new business concepts, such as Mobility-as-a-Service.

## Getting started

### Required Software

| Software  | Version   |
|---        |---        |
| node.js   | 18.13.0   |
| npm       | 8.19      |
| Hugo      | 0.110     |
| Git       | > 2.31    |

See our [Managing Required Software](https://gitlab.eclipse.org/eclipsefdn/it/webdev/hugo-solstice-theme/-/wikis/Managing-Required-Software) 
wiki page for more information on this topic. 

Install dependencies, build assets and start a webserver:

```bash
yarn && hugo server --disableFastRender --buildFuture
```

## Contributing

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [openmobility.eclipse.org](https://gitlab.eclipse.org/eclipsefdn/it/websites/openmobility.eclipse.org) repository
2. Clone repository: `git clone https://gitlab.eclipse.org/[your_eclipsefdn_username]/openmobility.eclipse.org.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request

### Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0

## Related projects

### [EclipseFdn/solstice-assets](https://gitlab.eclipse.org/eclipsefdn/it/webdev/solstice-assets)

Images, less and JavaScript files for the Eclipse Foundation look and feel.

### [EclipseFdn/hugo-solstice-theme](https://gitlab.eclipse.org/eclipsefdn/it/webdev/hugo-solstice-theme/)

Hugo theme of the Eclipse Foundation look and feel. 

## Bugs and feature requests

Have a bug or a feature request? Please search for existing and closed issues. If your problem or idea is not addressed yet, [please open a new issue](https://gitlab.eclipse.org/eclipsefdn/it/websites/openmobility.eclipse.org/-/issues).

## Author

**Christopher Guindon (Eclipse Foundation)**

- <https://twitter.com/chrisguindon>
- <https://github.com/chrisguindon>

## Trademarks

* Eclipse® is a Trademark of the Eclipse Foundation, Inc.
* Eclipse Foundation is a Trademark of the Eclipse Foundation, Inc.

## Copyright and license

Copyright 2018-2022 the [Eclipse Foundation, Inc.](https://www.eclipse.org) and the [openmobility.eclipse.org authors](https://gitlab.eclipse.org/eclipsefdn/it/websites/openmobility.eclipse.org/-/graphs/main). Code released under the [Eclipse Public License Version 2.0 (EPL-2.0)](https://gitlab.eclipse.org/eclipsefdn/it/websites/openmobility.eclipse.org/-/raw/main/README.md).
